{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module Grampa.Packrat.Text (parseFile) where

import Control.Applicative
import Text.Grampa
import Text.Grampa.PEG.Packrat qualified as P
import Data.Text qualified as T
import Data.Text.IO qualified as T
import Data.Char (digitToInt, isDigit, isSpace)
import Data.Function ((&))
import Data.Word (Word64)
import qualified Rank2.TH

import Expr

type Parser = P.Parser ExprGrammar T.Text

data ExprGrammar p = ExprGrammar {
  expr, prod, atom :: p Expr}

Rank2.TH.deriveAll ''ExprGrammar

instance TokenParsing (P.Parser ExprGrammar T.Text) where
   token = (<* takeCharsWhile isSpace)

productions :: GrammarBuilder ExprGrammar ExprGrammar P.Parser T.Text
productions ExprGrammar{..} = ExprGrammar{
  expr = chainRecursive (\r g-> g{expr= r}) prod $
         flip Bin <$> expr <*> (token $ Add <$ char '+' <|> Sub <$ char '-') <*> prod,
  prod = chainRecursive (\r g-> g{prod= r}) atom $
         flip Bin <$> prod <*> (token $ Mul <$ char '*' <|> Div <$ char '/') <*> atom,
  atom = Num . fromIntegral <$> token decimal <|> token (char '(') *> expr <* token (char ')')}

decimal :: Parser Word64
decimal = T.foldl' (\w d-> 10*w + (fromIntegral $ digitToInt d)) 0 <$> takeCharsWhile1 isDigit

grammar :: Grammar ExprGrammar P.Parser T.Text
grammar = fixGrammar productions

parseFile :: FilePath -> IO (Maybe Expr)
parseFile filepath = do
    content <- T.readFile filepath
    pure $
        expr (parseComplete grammar content) & \case
            Left err -> error (show err)
            Right a -> Just a
