{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE Strict #-}

module Grampa.PEG.ByteString (parseFile) where

import Control.Applicative
import Text.Grampa
import Text.Grampa.PEG.Backtrack qualified as PEG
import Text.Parser.Combinators (chainl1)
import Data.ByteString qualified as BS
import Data.Char (isDigit, isSpace)
import Data.Function ((&))
import Data.Monoid.Instances.ByteString.UTF8 qualified as B8
import Data.Word (Word64)
import Rank2 (Only)

import Expr

type Parser = PEG.Parser (Only Expr) B8.ByteStringUTF8

{-# INLINE expr #-}
expr :: Parser Expr
expr = chainl1 prod (Bin <$> op)
    where
        op = lexeme $ Add <$ char '+' <|> Sub <$ char '-'


{-# INLINE prod #-}
prod :: Parser Expr
prod = chainl1 atom (Bin <$> op)
    where
        op = lexeme $ Mul <$ char '*' <|> Div <$ char '/'


{-# INLINE atom #-}
atom :: Parser Expr
atom =
    Num <$> lexeme decimal
        <|> lexeme (char '(') *> expr <* lexeme (char ')')

{-# INLINE decimal #-}
decimal :: Parser Word64
decimal = BS.foldl' (\w d-> w + (fromIntegral d - 48) * 10) 0 . (\(B8.ByteStringUTF8 bs)-> bs) <$> takeCharsWhile1 isDigit


{-# INLINE lexeme #-}
lexeme :: Parser a -> Parser a
lexeme p = p <* takeCharsWhile isSpace


parseFile :: FilePath -> IO (Maybe Expr)
parseFile filepath = do
    content <- BS.readFile filepath
    pure $
        simply parseComplete expr (B8.ByteStringUTF8 content) & \case
            Left err -> error (show err)
            Right a -> Just a
