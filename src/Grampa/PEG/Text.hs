{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE Strict #-}

module Grampa.PEG.Text (parseFile) where

import Control.Applicative
import Text.Grampa
import Text.Grampa.PEG.Backtrack qualified as PEG
import Text.Parser.Combinators (chainl1)
import Data.Text qualified as T
import Data.Text.IO qualified as T
import Data.Char (digitToInt, isDigit, isSpace)
import Data.Function ((&))
import Data.Word (Word64)
import Rank2 (Only)

import Expr

type Parser = PEG.Parser (Only Expr) T.Text

{-# INLINE expr #-}
expr :: Parser Expr
expr = chainl1 prod (Bin <$> op)
    where
        op = lexeme $ Add <$ char '+' <|> Sub <$ char '-'


{-# INLINE prod #-}
prod :: Parser Expr
prod = chainl1 atom (Bin <$> op)
    where
        op = lexeme $ Mul <$ char '*' <|> Div <$ char '/'


{-# INLINE atom #-}
atom :: Parser Expr
atom =
    Num <$> lexeme decimal
        <|> lexeme (char '(') *> expr <* lexeme (char ')')

{-# INLINE decimal #-}
decimal :: Parser Word64
decimal = T.foldl' (\w d-> 10*w + (fromIntegral $ digitToInt d)) 0 <$> takeCharsWhile1 isDigit


{-# INLINE lexeme #-}
lexeme :: Parser a -> Parser a
lexeme p = p <* takeCharsWhile isSpace


parseFile :: FilePath -> IO (Maybe Expr)
parseFile filepath = do
    content <- T.readFile filepath
    pure $
        simply parseComplete expr content & \case
            Left err -> error (show err)
            Right a -> Just a
