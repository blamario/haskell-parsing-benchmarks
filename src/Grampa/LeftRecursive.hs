{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module Grampa.LeftRecursive (parseFile) where

import Control.Applicative
import Data.Char (digitToInt, isDigit, isSpace)
import Data.Function ((&))
import Data.Functor.Compose (Compose (getCompose))
import Data.Text qualified as T
import Data.Text.IO qualified as T
import Data.Word (Word64)
import qualified Rank2.TH
import Text.Grampa
import Text.Grampa.ContextFree.Memoizing.LeftRecursive qualified as P

import Expr

data ExprGrammar p = ExprGrammar {
  expr, prod, atom :: p Expr,
  decimal :: p Word64}

Rank2.TH.deriveAll ''ExprGrammar

instance TokenParsing (P.Parser ExprGrammar T.Text) where
   token = (<* takeCharsWhile isSpace)

productions :: GrammarBuilder ExprGrammar ExprGrammar P.Parser T.Text
productions ExprGrammar{..} = ExprGrammar{
  expr = flip Bin <$> expr <*> (token $ Add <$ char '+' <|> Sub <$ char '-') <*> prod <<|> prod,
  prod = flip Bin <$> prod <*> (token $ Mul <$ char '*' <|> Div <$ char '/') <*> atom <<|> atom,
  atom = Num <$> token decimal <<|> token (char '(') *> expr <* token (char ')'),
  decimal = T.foldl' (\w d-> 10*w + (fromIntegral $ digitToInt d)) 0 <$> takeCharsWhile1 isDigit}

grammar :: Grammar ExprGrammar P.Parser T.Text
grammar = P.autochain (fixGrammar productions)

parseFile :: FilePath -> IO (Maybe Expr)
parseFile filepath = do
    content <- T.readFile filepath
    pure $
        parseComplete grammar content & expr & getCompose & \case
            Left err -> error (show err)
            Right [a] -> Just a
            Right _ -> error "Ambiguous parses"
