{-# LANGUAGE Strict #-}

module Trifecta (parseFile) where

import Control.Applicative
import Data.Foldable (find)

import Data.ByteString qualified as BS
import Text.Trifecta

import Expr


{-# INLINE expr #-}
expr :: Parser Expr
expr = chainl1 prod (Bin <$> op)
    where
        op = token $ Add <$ char '+' <|> Sub <$ char '-'


{-# INLINE prod #-}
prod :: Parser Expr
prod = chainl1 atom (Bin <$> op)
    where
        op = token $ Mul <$ char '*' <|> Div <$ char '/'


{-# INLINE atom #-}
atom :: Parser Expr
atom =
    Num . fromIntegral <$> token decimal
    <|> token (char '(') *> expr <* token (char ')')


parseFile :: FilePath -> IO (Maybe Expr)
parseFile filepath = do
    content <- BS.readFile filepath
    pure (find (const True) $ parseByteString expr mempty content)
